import  {Component} from 'react';

import Environment from "../../environment/Environment";


class ActionHome extends Component {

    /**
     * @returns {Promise<void>}
     *
     *  esta funcion valida si en el local storage existe un token y consulata el servicio para validar si el token es valido
     *  en la url @URL_BASE/user-logged por metodo POST si se optiene un extatus code 200 se retorna el reultado del fech
     */
    async isLogged() {

        const token = localStorage.getItem('token');
        let logeddIn = null;

        if (token !== null) {
            await fetch(Environment.URL_BASE + 'user-logged', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    token: token
                })
            }).then((response) => {
                if (response.status === 200) {
                    return response.json();
                } else {
                    window.alert('Error, intentalo mas tarde');
                }
            }).then((responseJson) => {
                if (typeof responseJson !== 'undefined') {

                    return logeddIn = responseJson['response'];
                }
            });
        }

        return await logeddIn;
    }
}

export default ActionHome;
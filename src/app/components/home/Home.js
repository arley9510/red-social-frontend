import React, {Component} from 'react';

import ActionHome from './ActionHome';
import Login from "../login/Login";
import Main from '../main/Main';

class Home extends Component {

    state = {
        islogged: null,
    };

    /**
     * @returns {Promise<void>}
     *
     *
     */
    async componentWillMount() {
        const isLogged = new ActionHome();

        await isLogged.isLogged().then((response) => {

            return this.setState({islogged: response});
        });
    }

    /**
     * @returns {*}
     *
     *
     */
    render() {
        if (this.state.islogged === true) {
            return (
                <Main/>
            );
        } else {
            localStorage.clear();

            return (
                <Login/>
            );
        }
    }
}

export default Home;
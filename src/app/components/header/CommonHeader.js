import {Link} from 'react-router-dom'
import React from "react";

const CommonHeader = () => (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <a className="navbar-brand text-light">Red social</a>
        <ul className="nav">
            <li className="nav-item">
                <Link className="nav-link" to="login">Login</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="register">register</Link>
            </li>
        </ul>
    </nav>
);

export default CommonHeader;
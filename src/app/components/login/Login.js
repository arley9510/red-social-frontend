import React from "react";

import CommonHeader from "../header/CommonHeader";

const Login = () => (
    <div className="App-root">
        <CommonHeader/>
        <div className="App-login-background">
            <div className="container h-100">
                <div className="row h-100 justify-content-center align-items-center">
                    <form className="form-inline col-12">

                        <div className="form-group col-lg-5 col-md-5 col-xl-5">
                            <input type="email" className="form-control App-input-register" name="email"
                                   required
                                   placeholder="Email"/>
                        </div>

                        <div className="form-group col-lg-5 col-md-5 col-xl-5">
                            <input type="password" className="form-control App-input-register"
                                   name="password" required
                                   placeholder="Password"/>
                        </div>

                        <div className="form-group col-lg-2 col-md-2 col-xl-2">
                            <button className="btn btn-primary" onClick={() => login()}>
                                Login
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
);

const login = () => {
 window.alert('hola')
};

export default Login;
import React from "react";

import CommonHeader from "../header/CommonHeader";

const Register = () => (
    <div className="App-root">
        <CommonHeader/>
        <div className="App-register-background">
            <div className="container h-100">
                <div className="row h-100 justify-content-center align-items-center">
                    <form className="form-inline col-12">

                        <div className="form-group col-lg-3 col-md-3 col-xl-3">
                            <input type="text" className="form-control App-input-register" name="name"
                                   required
                                   placeholder="Name"/>
                        </div>

                        <div className="form-group col-lg-3 col-md-3 col-xl-3">
                            <input type="email" className="form-control App-input-register" name="email"
                                   required
                                   placeholder="Email"/>
                        </div>

                        <div className="form-group col-lg-3 col-md-3 col-xl-3">
                            <input type="password" className="form-control App-input-register"
                                   name="password" required
                                   placeholder="Password"/>
                        </div>

                        <div className="form-group col-lg-3 col-md-3 col-xl-3">
                            <input className="btn btn-primary" type="submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
);

export default Register;
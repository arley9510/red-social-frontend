import {Route} from 'react-router-dom';
import React, {Component} from 'react';
import Raven from 'raven-js';

import Home from './app/components/home/Home';
import Login from "./app/components/login/Login";
import Register from "./app/components/register/Register";

Raven.config('https://9ddc369963b24f159b0a76710b4ed5af@sentry.io/1199158').install();

class App extends Component {
    render() {
        return (
            <div className="App-root">
                <Route path="/" exact component={Home}/>
                <Route path="/login" exact component={Login}/>
                <Route path="/register" exact component={Register}/>
            </div>
        );
    }
}

export default App;